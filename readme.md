# Digilines global permanent memory controller

## Overview

Global digiline storage for variables of any kind (use with care).
based on: https://github.com/BuckarooBanzay/digiline_global_memory


## Usage

### Read and Write Private Information
Reading:
```lua
if event.type == "program" then
    digiline_send("channel", {
    command = "GET",
    name = "my_register"
    })
end

if event.type == "digiline" and event.channel == "channel" then
    print("Value: " .. event.msg)
end
```

Writing:
```lua
digiline_send("channel", {
    command = "SET",
    name = "my_register",
    value = 3.141
})
```

**NOTE**: the memory is bound to the user who placed it, the same register can only be accessed if it is placed by the same player


### Read and Write public Information

**NOTE**: this is experimental feature. This needs keen testing. 
Reading:
```lua
if event.type == "program" then
    digiline_send("channel", {
    command = "GET",
    type="public", -- if private this is omitted
    key="1234",  -- key to store data
    name = "my_register" -- name of data
    })
end

if event.type == "digiline" and event.channel == "channel" then
    print("Value: " .. event.msg)
end 
```

Writing:
```lua


if event.type == "program" then

digiline_send("channel", {
    command = "SET",
    type="public",  
    rtype="r", -- it is must for first time. if nothing given it will be "rw". this can only be modified by the owner of the data
    key="1234", -- key
    name = "my_register",
    value = 3123213
})

else
    digiline_send("t",event.msg) --to catch the message. it will tell if write success or failiure  

end

```

**NOTE**: modifying rtype (read-type) is planned to be a feature soon.





# Chatcommands

* **/digiline_global_memory [name]** Returns the contents of the current players memory with given name
* **/digiline_global_memory_clear** Clears the current players memory

# TODO

* [ ] memory constraints.
* [x] crafting recipe.
* [x] persistence.
* [x] privs for deleting others memory/writing into others memory.
* [x] Allow cleaning of memory with one command.
* [x] Do not allow functions data type.
* [ ] Optmizations to mod.
# License

* textures/global_memory_controller_top.png
    * CC BY-SA 3.0 https://cheapiesystems.com/git/digistuff