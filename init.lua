
local MP = minetest.get_modpath("digiline_permament_global_storage")
print("[INFO] Mod has been loaded   ")
storage = minetest.get_mod_storage()
digiline_global_memory = {
    store = {},-- playername -> memory_name -> value
    public = {}  -- keyname -> {memory_name -> value,player->r/rw} -- r for read only rw for read and write
    }

function initial()
    a = storage:get_string("global_memory")
    if a == nil then
        minetest.log("action","Creating new mod storage")
        digiline_global_memory = {
        store = {} ,
        public = {}
        }
        dofile(MP.."/functions.lua")
        storage:set_string("global_memory",minetest.serialize(digiline_global_memory))

    else
        minetest.log("action","got global_memory")
        digiline_global_memory =  minetest.deserialize(storage:get_string("global_memory")) or {
            store = {} ,
            public = {}
            }
            dofile(MP.."/functions.lua")
    end

end


minetest.register_on_shutdown(function()
    storage:set_string("global_memory",minetest.serialize(digiline_global_memory))
end)


dofile(MP.."/functions.lua")
dofile(MP.."/controller.lua")
dofile(MP.."/chatcommands.lua")
initial()
