
digiline_global_memory.get_value = function(playername, memory_name)
    local player_storage = digiline_global_memory.store[playername]
    if not player_storage then
      player_storage = {}
      digiline_global_memory.store[playername] = player_storage
    end
  
    return player_storage[memory_name]
  end
  
  digiline_global_memory.set_value = function(playername, memory_name, value)
    local player_storage = digiline_global_memory.store[playername]
    if not player_storage then
      player_storage = {}
      digiline_global_memory.store[playername] = player_storage
    end
  
    player_storage[memory_name] = value

  end
  


digiline_global_memory.set_value_public = function(playername, key ,memory_name, value,acess)

  local player_storage = digiline_global_memory.public[key]
  if not player_storage then
    player_storage = {}
    digiline_global_memory.public[key] = player_storage
    player_storage.owner = playername
  end
  local has_write, missing = minetest.check_player_privs(name, {digiline_memory_admin = true})
  if not player_storage.acess then
    player_storage.acess = acess
  elseif player_storage.acess == "rw" or player_storage.owner== playername or has_write then
    player_storage[memory_name] = value
    return "Sucess"
  else 
    return "cannot store in the read only key"
  end 
end

digiline_global_memory.change_access = function(playername, key ,acess)
  --[[
    to let the owner change read type(Access type) ]]

    local player_storage = digiline_global_memory.public[key]
    local has_admin, missing = minetest.check_player_privs(name, {digiline_memory_admin = true})
    if not player_storage then
      return "No Data Present"
    end
    if has_admin or player_storage.owner== playername then
      player_storage[memory_name] = value
      return "Sucess"
    else 
      return "You are disallowed to modify the key."
    end
end

digiline_global_memory.get_value_public = function(key,memory_name)
  local player_storage = digiline_global_memory.public[key]
  if not player_storage then
    player_storage = {}
    digiline_global_memory.store[key] = player_storage
  end

  return player_storage[memory_name]
end

digiline_global_memory.delete_public = function(playername,key)
  local player_storage = digiline_global_memory.public[key]
  if not player_storage then
    return  "Key not found"
  end
  local has_write, missing = minetest.check_player_privs(name, {digiline_memory_admin = true})
  if playername == player_storage.owner or has_Write then
    digiline_global_memory.public[key] = nil
    return "Sucess"
  else
    return "Key can only be deleted by the owner or person with digiline_memory_admin  priv"
  end
end

digiline_global_memory.delete_private = function(playername)
  local player_storage = digiline_global_memory.store[playername]
  if not player_storage then
    return  "Data not found"
  end
    digiline_global_memory.public[key] = nil
    return "Sucess"
 
end