
  minetest.register_privilege("digiline_memory_admin", {
    description = "Can clear memory and bypass player restrictions",
    give_to_singleplayer = true
})
minetest.register_privilege("digiline_memory_write", {
  description = "Can write into digiline",
  give_to_singleplayer = true
})
minetest.register_chatcommand("digiline_global_memory", {
    params = "<name>",
      description = "shows the current memory value",
      
      func = function(name, param)
      if param == "" or not param then
        return false, "Please specify the memory name!"
      end
  
      return true, dump(digiline_global_memory.get_value(name, param) or "<empty>")
    end
  })
  
  minetest.register_chatcommand("digiline_private_memory_clear", {
    params = "<name>",
      description = "clears the private global memory of the player.",
      privs = {
        digiline_memory_admin = true,
        
     },
      func = function(name)
      local response = digiline_global_memory.delete_private(name)
      if response == "Sucess" then
        return true, "Memory cleared"
      else
        return true, response
      end
    end
  })
 
minetest.register_chatcommand("digiline_public_memory_clear", {
  params = "<name>",
    description = "clears the public global memory of the player.",
    privs = {
      digiline_memory_admin = true,
      
    },
    func = function(name)
    local response = digiline_global_memory.delete_public(name)
    if response == "Sucess" then
      return true, "Memory cleared"
    else
      return true, response
    end
  end
})
minetest.register_chatcommand("digiline_memory_clear_all", {
  params = "<name>",
    description = "clears the public global memory of the player.",
    privs = {
      digiline_memory_admin = true,
      
    },
    func = function(name)
      digiline_global_memory.store = {}
      digiline_global_memory.public = {}
  end
})
